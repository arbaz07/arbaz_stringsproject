const string1 = require("./string1.cjs");
const string2 = require("./string2.cjs");
const string3 = require("./string3.cjs");
const string4 = require("./string4.cjs");
const string5 = require("./string5.cjs");
const data1 = {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"};

//console.log(string1("-$100.45"))

console.log(string1(data1));

console.log(string2(data1));
console.log(string3(data1));
console.log(string4(data1));
console.log(string5(data1));
