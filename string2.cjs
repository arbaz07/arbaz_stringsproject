
function string2(str){
    if(typeof(str)=='string'){
        let ipArr = str.split('.');
        let numArray = ipArr.map(Number);
        if(ipArr.length > 4){
            return [];
        }
        for(i=0; i<ipArr.length; i++){
            
            if(numArray[i]==NaN || numArray[i]>255 || numArray[i]<0){
                return []
            }
        }
        return numArray;
    }
    return [];

}

module.exports = string2;