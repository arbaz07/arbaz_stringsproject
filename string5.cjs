const _ = require("underscore");

function string5(arrayString){
    let sentence = "";
    if(_.isArray(arrayString)){
        for (i=0; i<arrayString.length; i++){
            sentence+=arrayString[i];
            if(i!=arrayString.length-1){
                sentence+=" ";
            }
        }
        // sentence.replace(sentence.length,".");
        return sentence+".";
    }
    return "";
    
}

module.exports = string5;