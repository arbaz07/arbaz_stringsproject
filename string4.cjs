// const _ = require('underscore');






function string4(obj){
    if(typeof(obj)=='object'){
        let fname="";
        let mname="";
        let lname="";
        
        if(obj.hasOwnProperty('first_name')){
            const Fname = obj.first_name;
            //console.log(Fname);
            fname =makeItTitleCase(Fname);
        }
        if(obj.hasOwnProperty('middle_name')){
            const Mname = obj.middle_name;
            //console.log(Mname);
            mname =makeItTitleCase(Mname);
        }
        if(obj.hasOwnProperty('last_name')){
            const Lname = obj.last_name;
            //console.log(Lname);
            lname =makeItTitleCase(Lname);
        }
        return fname+" "+mname+" "+lname;
    }
    return "";

}

function makeItTitleCase(str){
    return str.charAt(0).toUpperCase() + str.substring(1,str.length).toLowerCase();
}


// function name() {
//     let text = "dsfsfhajbfSSSSsfbasdfdsf!";
//     let result = makeItTitleCase(text);  
//     return result;  
// }




module.exports = string4;