
const monthNames = ["January", "February", "March", "April", "May", "June",
"July", "August", "September", "October", "November", "December"
];


function string3 (dateStr){
    if(typeof(dateStr) == 'string'){
        const date = new Date(dateStr);
        return monthNames[date.getMonth()];
    }
    return "";
}

module.exports = string3;